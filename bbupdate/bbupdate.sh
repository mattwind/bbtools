#!/bin/bash

IP="$1."
START=160;
END=165;

USERNAME=root
PASSWORD=derpherp

find_mac=("90:59" "68:1C" "C8:A0")

for i in $(seq $START $END); 
do

	echo "Scanning $IP$i";

	PING=`ping -c 1 -W 1 $IP$i`
	NMAP=`nmap -p T:22 $IP$i`
	PORT22=`echo $NMAP | grep "22/tcp open"`
	MAC=`echo $NMAP | grep "MAC Address:" | awk -F"Address: " '{print $2}' | cut -d" " -f1`
	MAC_PREFIX=`echo $MAC | cut -c1-5`

	if [ -n "$MAC_PREFIX" ]
	then

		# find mac
		for x in "${find_mac[@]}";
		do

			if [ "$MAC_PREFIX" == "$x" ]
			then
				echo "	$MAC Aggregator Found";

				if [ -n "$PORT22" ]
				then
					echo "	Port 22 Open"
					sshpass -p $PASSWORD scp -o "StrictHostKeyChecking no" ./motd $USERNAME@$IP$i:/etc/motd
					echo "	Files updated"
				fi
			fi
		done
	fi
	
done

