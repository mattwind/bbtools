#!/bin/bash

date=`date +%Y%m%d%H%M%S`
counter=0
tests=5
delay=5
wap_distance=0
D=","

getUserInput() {

	read -p "Enter the Wireless model: " wireless_model
	read -p "Enter the WAP model: " wap_model
	read -p "Enter the WAP IP: " wap_ip
	read -p "Enter the current distance in feet from WAP: " wap_distance
	read -p "How many test scans? " tests
	read -p "Scan delay in seconds? " delay
	read -p "Continue? (y/n) " continue_test

	if [ "$continue_test" == "y" ]
	then
		doTest
	else
		getUserInput
	
	fi

}

function doTest() {

	test_filename="$wap_model.csv"

	if [ ! -f "$test_filename" ]
	then
		touch $test_filename
		header="model,distance,link,level,noise,ping min,ping avg,ping max"
		echo -e $header > $test_filename
	fi

	while [ $counter -lt $tests ]; do
		ts=`date +%M%S`
		counter=$[$counter +1]
		scan_results=`cat /proc/net/wireless | grep wlan0`
		ping_results=`ping -c 1 -W $delay $wap_ip | grep rtt | cut -d" " -f4`
		ping_min=`echo $ping_results | cut -d"/" -f1`
		ping_avg=`echo $ping_results | cut -d"/" -f2`
		ping_max=`echo $ping_results | cut -d"/" -f3`
		status=`echo $scan_results | cut -d" " -f2`
		link=`echo $scan_results | cut -d" " -f3 | tr -d '.'`
		level=`echo $scan_results | cut -d" " -f4 | tr -d '.'`
		noise=`echo $scan_results | cut -d" " -f5 | tr -d '.'`
		results="$wireless_model$D$wap_distance$D$link$D$level$D$noise$D$ping_min$D$ping_avg$D$ping_max"
		echo [$counter] $results
		echo -e $results >> $test_filename
		sleep $delay
	done

	echo "Test complete!"
	echo $test_filename

}

getUserInput
