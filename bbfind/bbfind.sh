#!/bin/bash

tmp_file=/tmp/nmap.tmp
list_file=/tmp/nmap.lst

rm $tmp_file
rm $list_file

network=$1

# mac array
find_mac=("90:59" "68:1C" "C8:A0" "68:1c" "c8:a0")
find_mac_counter=0

# build list

if [ "$network" == "" ]
then 
	network="172.16.0.0"
fi

nmap -sP $network/24 > $tmp_file

declare -a nmap_array=($(
	grep -e report -e MAC $tmp_file | \
	sed -e '{
		s/Nmap scan report for //g
		s/MAC Address: //g
		s/ (.\+//g
	}'
))

nmap_array_len=${#nmap_array[@]}

echo -n > "$list_file"

for (( i = 0; i <= $nmap_array_len; i++ )); do

	while (( $(echo ${nmap_array[$i]} | grep -c '[0-9A-F]\+:') < 1 )); do
	(( i++ ))
	done
	
	mac4=`echo ${nmap_array[$i]} | cut -c1-5`

	# find mac
	for x in "${find_mac[@]}"
	do
	# do stuff
		if [ "$mac4" == "$x" ]
		then
			echo ${nmap_array[$i]} ${nmap_array[(($i - 1 ))]}  >> "$list_file"
			(( find_mac_counter++ ))
		fi
	  done
	 (( i++ ))
done

# show list
echo
cat $list_file
echo
echo "$find_mac_counter found on the $network network"
echo
exit 0
