# Find Beagle Bones on a Network

Output a simple list of devices based on the BeagleBone MAC Address (Physical Address)

USAGE

	./bbfind.sh 172.16.0.0

DEPENDENCIES

This script requires nmap, and must be executed by root.

TODO

* Use external file for storing result MAC Addresses
* Allow configurable delimiter
* Help Output
* Progress Output
* Check if tmp file exist before removing
